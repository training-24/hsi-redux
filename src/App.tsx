import { ThunkDispatch } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import {  RootState } from './main.tsx';
import { selectTheme } from './store/config/config.selectors.ts';
import { changeLanguage, changeTheme } from './store/config/config.store.ts';
import { dec, inc } from './store/counter/counter.actions.ts';
import { getProducts } from './store/users/users.actions.ts';

function App() {
  const theme = useSelector(selectTheme)
  const dispatch = useDispatch() as ThunkDispatch<any, any, any>;
  
  return (
    <>
      <ThemeViewer />

      <hr/>
      <button onClick={() => dispatch(getProducts())}>getProducts</button>

      <button onClick={() => dispatch(dec(5))}>-</button>
      <button onClick={() => dispatch(inc())}>+</button>
      <input type="text" value={theme} onChange={(e) => {
        dispatch(changeTheme(e.target.value as any))

      }}/>
      <hr/>
      <button onClick={() => dispatch(changeTheme('dark'))}>dark</button>
      <button onClick={() => dispatch(changeTheme('light'))}>light</button>
      <button onClick={() => dispatch(changeLanguage('it'))}>it</button>
      <button onClick={() => dispatch(changeLanguage('en'))}>en</button>
    </>
  )
}

export default App


function ThemeViewer() {
  const theme = useSelector((state: RootState) => state.config.theme)
  return <div>
    <div>theme viewer: {theme}</div>
  </div>
}
