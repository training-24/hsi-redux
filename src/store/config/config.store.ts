import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type Language = 'it' | 'en';
type Theme = 'dark' | 'light'

type ConfigState = {
  language: Language,
  theme: Theme
}

const initialState: ConfigState = {
  language: 'it',
  theme: 'dark'
}
export const configStore = createSlice({
  name: 'config',
  initialState,
  reducers: {
    changeTheme(state, action: PayloadAction<Theme> ) {
      state.theme = action.payload;
    },
    changeLanguage(state, action: PayloadAction<Language> ) {
      state.language = action.payload;
    },
  }
})

export const {
  changeTheme,
  changeLanguage
} = configStore.actions
