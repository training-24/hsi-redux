import { RootState } from '../../main.tsx';

export const selectTheme = (state: RootState) => state.config.theme
