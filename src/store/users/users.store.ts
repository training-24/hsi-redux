import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState: any[] = []

export const usersStore = createSlice({
  name: 'users',
  initialState,
  reducers: {
    getUsersSuccess(state, action: PayloadAction<any[]> ) {
      // state.list = action.payload
      return action.payload;
    },
    addUsersSuccess(state, action: PayloadAction<any> ) {
      state.push(action.payload)
    },
    clear() {
      return []
    },
  },
})

export const {
  getUsersSuccess, clear,
} = usersStore.actions
