import { ThunkAction } from '@reduxjs/toolkit';
import axios from 'axios';
import { RootState } from '../../main.tsx';
import { getUsersSuccess } from './users.store.ts';

export const getProducts = (): ThunkAction<any, RootState, any, any> =>  {
  return function(dispatch, getState, extraArgument) {
    dispatch({ type: 'get products'})

      // http.ge
    // dispatch success
    axios.get<any[]>('https://jsonplaceholder.typicode.com/users')
      .then((res) => {
        dispatch(getUsersSuccess(res.data))
      })
    // getState().

  }
}
