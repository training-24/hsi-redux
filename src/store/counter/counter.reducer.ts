import { createReducer } from '@reduxjs/toolkit';
import { dec, inc } from './counter.actions.ts';

const initialState: number = 0;

export const counterReducer = createReducer(initialState, builder =>
  builder
    .addCase(inc, (state) => state + 1)
    .addCase(dec, (state, action) =>  state - action.payload)
)
/*

export function counterReducer(state = 0, action: any) {
  switch (action.type) {
    case 'inc':
      return state + 1

    case dec.type:
      return state - action.payload;
  }
  return state;
}
*/
