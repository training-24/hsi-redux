import { createAction } from '@reduxjs/toolkit';
export const dec = createAction<number>('counter/dec')
export const inc = createAction('counter/inc')
