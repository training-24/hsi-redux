import { combineReducers, configureStore } from '@reduxjs/toolkit';
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux';
import App from './App.tsx'
import './index.css'
import { configStore } from './store/config/config.store.ts';
import { counterReducer } from './store/counter/counter.reducer.ts';
import { usersStore } from './store/users/users.store.ts';

export const rootReducer = combineReducers({
  todos: () => [1, 2, 3 ],
  counter: counterReducer,
  config: configStore.reducer,
  users: usersStore.reducer
})

export const store = configureStore({
  reducer: rootReducer,
})

export type RootState = ReturnType<typeof rootReducer>


ReactDOM.createRoot(document.getElementById('root')!).render(
  <Provider store={store}>
    <App />
  </Provider>,
)

